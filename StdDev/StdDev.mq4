//+------------------------------------------------------------------+
//|                                           Standard Deviation.mq4 |
//|                      Copyright © 2005, MetaQuotes Software Corp. |
//|                                       http://www.metaquotes.net/ |
//+------------------------------------------------------------------+
#property copyright "Copyright © 2005, MetaQuotes Software Corp."
#property link      "http://www.metaquotes.net/"
#property strict

#property indicator_separate_window
#property indicator_minimum 0
#property indicator_buffers 1
#property indicator_plots 1
#property indicator_color1 Blue

//---- input parameters
input int ExtStdDevPeriod = 20;
input int ExtStdDevMAMethod = 0;
input int ExtStdDevAppliedPrice = 0;
input int ExtStdDevShift = 0;

//---- buffers
double ExtStdDevBuffer[];

//+------------------------------------------------------------------+
//| Custom indicator initialization function                         |
//+------------------------------------------------------------------+
void OnInit()
{
	string sShortName;

	//---- indicator buffer mapping
	SetIndexBuffer(0, ExtStdDevBuffer);
	ArraySetAsSeries(ExtStdDevBuffer, true); // for 5

	//---- indicator line
	SetIndexStyle(0, DRAW_LINE);

	//---- line shifts when drawing
	SetIndexShift(0, ExtStdDevShift);

	//---- name for DataWindow and indicator subwindow label
	sShortName = "StdDev(" + (string)ExtStdDevPeriod + ")";
	IndicatorSetString(INDICATOR_SHORTNAME, sShortName);
	SetIndexLabel(0, sShortName);

	//---- first values aren't drawn
	SetIndexDrawBegin(0, ExtStdDevPeriod);
}

//+------------------------------------------------------------------+
//| Standard Deviation                                               |
//+------------------------------------------------------------------+
int OnCalculate(const int rates_total, const int prev_calculated, const datetime& time[], const double& open[], const double& high[], const double& low[], const double& close[], const long& tick_volume[], const long& volume[], const int& spread[])
{
	ArraySetAsSeries(open, true);
	ArraySetAsSeries(high, true);
	ArraySetAsSeries(low, true);
	ArraySetAsSeries(close, true);

	int    i, j, nCountedBars;
	double dAPrice, dAmount, dMovingAverage;
	
	//---- insufficient data
	if (rates_total <= ExtStdDevPeriod)
		return(0);

	//---- bars count that does not changed after last indicator launch.
	nCountedBars = prev_calculated;

	//----Standard Deviation calculation
	i = rates_total - ExtStdDevPeriod - 1;
	
	if (nCountedBars > ExtStdDevPeriod)
		i = rates_total - nCountedBars;

	while (i >= 0)
	{
		dAmount = 0.0;
		dMovingAverage = MA(NULL, 0, ExtStdDevPeriod, 0, (ENUM_MA_METHOD)ExtStdDevMAMethod, ExtStdDevAppliedPrice, i);
		
		if (dMovingAverage == 0.0)
			return(rates_total - i - 1);

		for (j = 0; j < ExtStdDevPeriod; j++)
		{
			dAPrice = GetAppliedPrice(ExtStdDevAppliedPrice, i + j, open, high, low, close);
			dAmount += (dAPrice - dMovingAverage) * (dAPrice - dMovingAverage);
		}

		ExtStdDevBuffer[i] = MathSqrt(dAmount / ExtStdDevPeriod);
		i--;
	}

	//----
	return(rates_total);
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double GetAppliedPrice(int nAppliedPrice, int nIndex, const double& open[], const double& high[], const double& low[], const double& close[])
{
	double dPrice;
	
	//----
	switch (nAppliedPrice)
	{
		case 0:
			dPrice = close[nIndex];
			break;

		case 1:
			dPrice = open[nIndex];
			break;

		case 2:
			dPrice = high[nIndex];
			break;

		case 3:
			dPrice = low[nIndex];
			break;

		case 4:
			dPrice = (high[nIndex] + low[nIndex]) / 2.0;
			break;

		case 5:
			dPrice = (high[nIndex] + low[nIndex] + close[nIndex]) / 3.0;
			break;

		case 6:
			dPrice = (high[nIndex] + low[nIndex] + 2 * close[nIndex]) / 4.0;
			break;

		default:
			dPrice = 0.0;
	}

	//----
	return(dPrice);
}

#ifndef __MQL4__

// only 2 arguments for example
void SetIndexStyle(int index, int type)
{
	PlotIndexSetInteger(index, PLOT_DRAW_TYPE, type);
}

void SetIndexShift(int index, int shift)
{
	PlotIndexSetInteger(index, PLOT_SHIFT, shift);
}

void SetIndexLabel(int index, string text)
{
	PlotIndexSetString(index, PLOT_LABEL, text);
}

void SetIndexDrawBegin(int index, int begin)
{
	PlotIndexSetInteger(index, PLOT_DRAW_BEGIN, begin);
}

#endif


double MA(string symbol, ENUM_TIMEFRAMES timeframe, int ma_period, int ma_shift, ENUM_MA_METHOD ma_method, int applied_price, int shift)
{
#ifdef __MQL4__

	return(iMA(symbol, timeframe, ma_period, ma_shift, ma_method, applied_price, shift));

#else

	static int ma_handle = INVALID_HANDLE;
	
	if (ma_handle == INVALID_HANDLE)
		ma_handle = iMA(symbol, timeframe, ma_period, ma_shift, ma_method, applied_price);
	
	if (ma_handle == INVALID_HANDLE)
		return(0);

	double values[];
	return(CopyBuffer(ma_handle, 0, shift, 1, values) == 1 ? values[0] : 0);

#endif
}



//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
